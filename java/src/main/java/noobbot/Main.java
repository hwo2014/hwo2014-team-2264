package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.DecimalFormat;
import java.sql.Timestamp;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.JsonObject;
import com.google.gson.JsonElement;

public class Main {
    static final boolean DEBUG_TRACK = false;
    static final boolean DEBUG_SPEED = true;
    static final boolean DEBUG_SENT = true;
    private final boolean preferInnerLane = true;//false=outerLane

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        if (args.length > 7) {
            String type = args[4];
            String track = args[5];
            int cars = Integer.parseInt(args[6]);
            String pass = ("".equals(args[7]) ? null : args[7]);
            new Main(reader, writer, new NewRace(botName, botKey, type, track, cars, pass));
        } else {
            new Main(reader, writer, new Join(botName, botKey));
        }
    }

    final Gson gson = new Gson();
    final JsonParser jsonParser = new JsonParser();
    private PrintWriter writer;
    private TrackInformation trackInformation; // constant
    private CarPositions carPositions; // changes on every tick
    private CarPositions carPositionsPrev; // car positions on previous tick
    private CarId ownCar;
    static boolean switchCommandSent = false;
    static boolean laneIsChanging = false;
    static boolean crashed = false;
    static boolean finished = false;

    static double friction50    = 1.0;
    static double friction100   = 1.0;
    static double friction200   = 1.0;

    private static int gameTick = 0;

    double previousSpeed = 0; //for crash analysis

    public Main(final BufferedReader reader, final PrintWriter writer, final SendMsg join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);

        while ((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = (MsgWrapper) gson.fromJson(line, MsgWrapper.class);

            JsonObject jsonobj = (JsonObject) jsonParser.parse(line);
            JsonElement je = jsonobj.get("gameTick");
            if (je != null) {
                gameTick = Integer.parseInt(je.toString());
            }

            if (msgFromServer.msgType.equals("carPositions")) {
                carPositionsPrev = carPositions;
                carPositions = (CarPositions) gson.fromJson(line, CarPositions.class);

                CarPositions.PiecePosition current = carPositions.getCarPosition(ownCar).piecePosition;
                int targetLane;
                if (needToOvertake()) {
                    targetLane = trackInformation.getOptimalLaneBetweenSwitches(
                        trackInformation.getNextSwitchIndex(current.pieceIndex), !preferInnerLane);
                    switchCommandSent = false;
                } else {
                    targetLane = trackInformation.getOptimalLaneBetweenSwitches(
                        trackInformation.getNextSwitchIndex(current.pieceIndex), preferInnerLane);
                }

                if (current.lane.startLaneIndex < targetLane && !current.lane.isChanging() && !switchCommandSent) {
                    send(new SwitchLane(SwitchLane.Direction.Right));
                } else if (current.lane.startLaneIndex > targetLane && !current.lane.isChanging() && !switchCommandSent) {
                    send(new SwitchLane(SwitchLane.Direction.Left));
                } else {
                    controlThrottle();
                }

                if (!laneIsChanging && current.lane.isChanging()) laneIsChanging = true;
                if (laneIsChanging && !current.lane.isChanging()) {
                    laneIsChanging = false;
                    switchCommandSent = false;
                }
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("yourCar")) {
                JsonObject jo = (JsonObject) jsonParser.parse(line);
                ownCar = (CarId) gson.fromJson(jo.get("data").toString(), CarId.class);
            } else if (msgFromServer.msgType.equals("gameInit")) {
                trackInformation = (TrackInformation) gson.fromJson(line, TrackInformation.class);
                System.out.println("Track: " + trackInformation.data.race.track.name);
                System.out.println("Race participants:");
                for (int i = 0; i < trackInformation.data.race.cars.size(); i++) {
                    System.out.println(" " + (i+1) + ". " + trackInformation.data.race.cars.get(i).id.name);
                }
                System.out.println("Starting time: " + new Timestamp(new java.util.Date().getTime()));
                System.out.println("-----------------");

                if (DEBUG_TRACK) {
                    for (int i = 0; i < trackInformation.getPieces().size(); i++) {
                        System.out.println("Piece " + i + ": " + trackInformation.getPieces().get(i));
                    }
                }
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
                System.out.println(line);
                send(new Throttle(1.0));
                switchCommandSent = false;// one carPositions and thus switchLane is sent before gameStart
            } else if (msgFromServer.msgType.equals("lapFinished")) {
                LapFinished lf = (LapFinished) gson.fromJson(line, LapFinished.class);
                if (ownCar.equals(lf.data.car)) {
                    System.out.println("Lap " + (lf.data.lapTime.lap + 1) + " finished, "
                        + "ranking #" + lf.data.ranking.overall + ", "
                        + "lap time " + lf.data.lapTime.millis/1000.0 + " s");
                }
            } else if (msgFromServer.msgType.equals("finish")) {
                JsonObject jo = (JsonObject) jsonParser.parse(line);
                CarId finishedCar = gson.fromJson(jo.get("data").toString(), CarId.class);
                if (ownCar.equals(finishedCar)) {
                    finished = true;
                }
                System.out.println(line);
            } else if (msgFromServer.msgType.equals("crash")) {
                JsonObject jo = (JsonObject) jsonParser.parse(line);
                CarId crashedCar = gson.fromJson(jo.get("data").toString(), CarId.class);
                if (ownCar.equals(crashedCar)) {
                    crashed = true;
                    System.out.println("crash at gameTick " + jo.get("gameTick").toString() + " in carPosition "
                        + gson.toJson(carPositions.getCarPosition(crashedCar)));

                    // go slower next time
                    int pieceIndex = carPositions.getCarPosition(crashedCar).piecePosition.pieceIndex;
                    double radius = trackInformation.getPiece(pieceIndex).radius;
                    if      (Math.abs(radius) < 60)  friction50  -= 0.05;
                    else if (Math.abs(radius) < 110) friction100 -= 0.05;
                    else if (Math.abs(radius) < 210) friction200 -= 0.05;

                } else {
                    System.out.println(line);
                }
            } else if (msgFromServer.msgType.equals("spawn")) {
                JsonObject jo = (JsonObject) jsonParser.parse(line);
                CarId spawnedCar = gson.fromJson(jo.get("data").toString(), CarId.class);
                if (ownCar.equals(spawnedCar)) {
                    crashed = false;
                }
                System.out.println(line);
            } else {
                // print all unknown/unhandled messages
                System.out.println(line);
            }
        }
    }

    public static int getGameTick() {
        return gameTick;
    }

    private boolean needToOvertake() {
        CarPositions.PiecePosition ownPosition = carPositions.getCarPosition(ownCar).piecePosition;
        if (!getPieceAtDistance(ownPosition.pieceIndex, 1).isSwitch) {
            return false;
        }

        // pick some car's dimensions and hope they are all equal :)
        TrackInformation.Dimension car = trackInformation.data.race.cars.get(0).dimensions;

        double maxDistance = 2 * car.length;
        double distance = 0;
        int maxPiece = 0;
        double maxPositionAtPiece = 0;
        boolean finishLine = false;
        int i = 0;
        while (distance < maxDistance) {
            double tmp = getPieceAtDistance(ownPosition.pieceIndex, i).getLaneLength(trackInformation, ownPosition.lane);
            if (i == 0) tmp -= ownPosition.inPieceDistance;
            if (distance + tmp < maxDistance) {
                distance += tmp;
            } else {
                maxPositionAtPiece = maxDistance - distance;
                distance = maxDistance;
                maxPiece = ownPosition.pieceIndex + i;
                if (maxPiece >= trackInformation.getPieces().size()) {
                    maxPiece -= trackInformation.getPieces().size();
                    finishLine = true;
                }
            }
            i++;
        }

        CarPositions.PiecePosition otherPosition;
        for (int j = 0; j < carPositions.data.size(); j++) {
            if (carPositions.data.get(j).id.equals(ownCar)) {
                continue;
            }
            otherPosition = carPositions.getCarPosition(carPositions.data.get(j).id).piecePosition;

            boolean overtake;
            if (otherPosition.pieceIndex == ownPosition.pieceIndex
                    && otherPosition.inPieceDistance > ownPosition.inPieceDistance) {
                // we must be close enough when driving on same piece :)
                overtake = true;
            } else if (otherPosition.pieceIndex == maxPiece
                    && otherPosition.inPieceDistance <= maxPositionAtPiece) {
                overtake = true;
            } else if (finishLine && otherPosition.pieceIndex < maxPiece) {
                overtake = true;
            } else if (otherPosition.pieceIndex > ownPosition.pieceIndex
                    && otherPosition.pieceIndex < maxPiece) {
                overtake = true;
            } else {
                overtake = false;
            }

            // assumes that both cars are on the same lane
            if (ownPosition.lane.equals(otherPosition.lane) && overtake) {
                return true;
            }
        }
        return false;
    }

    private TrackInformation.Piece getPieceAtDistance(int currentPieceIndex, int distance) {
        int pieces = trackInformation.getPieces().size();
        TrackInformation.Piece piece = (
                (currentPieceIndex + distance < pieces)
                ? trackInformation.getPiece(currentPieceIndex + distance)
                : trackInformation.getPiece(currentPieceIndex + distance - pieces));
        return piece;
    }

    private boolean isOwnCarSliding(double maxAngle) {
        return Math.abs(carPositions.getAngle(ownCar)) > Math.abs(maxAngle);
    }

    private double getSpeed(CarId id) {
        if (carPositionsPrev == null || id == null) {
            return 0.0;
        }

        int previousTick = carPositionsPrev.gameTick;
        int currentTick = carPositions.gameTick;
        CarPositions.PiecePosition previous = carPositionsPrev.getCarPosition(id).piecePosition;
        CarPositions.PiecePosition current = carPositions.getCarPosition(id).piecePosition;
        double angle = carPositions.getCarPosition(id).angle;

        double speed;
        if (current.pieceIndex == previous.pieceIndex) {
            speed = (current.inPieceDistance - previous.inPieceDistance) / (currentTick - previousTick);
        } else {
            TrackInformation.Piece previousPiece = trackInformation.getPiece(previous.pieceIndex);
            double distance = current.inPieceDistance
                    + previousPiece.getLaneLength(trackInformation, previous.lane)
                    - previous.inPieceDistance;
            speed = distance / (currentTick - previousTick);
        }

        if (Math.abs(speed - previousSpeed) > 0.5 && !crashed) {
            System.out.println("SPEEDOMETER DEBUG");
        }
        previousSpeed = speed;

        DecimalFormat df = new DecimalFormat("##.####");
        if (DEBUG_SPEED && !finished) {
            System.out.println("GameTick=" + currentTick + ": angle=" + df.format(angle) +
                    ", speed=" + df.format(speed) + " at " + gson.toJson(current) +
                    ", piece={" + trackInformation.getPiece(current.pieceIndex) + "}");
        }

        return speed;
    }

    public static double getSpeedLimit(double radius) {
        if (radius == 0.0)               return 15.0;              // Straight, "no limit"
        if (Math.abs(radius) < 60)       return 4.7 * friction50;  // 50
        else if (Math.abs(radius) < 110) return 6.8 * friction100; // 100
        else if (Math.abs(radius) < 210) return 9.5 * friction200; // 200
        else return 12.0;                // there should not even be Pieces like this :)
    }

    private void controlThrottle() {
        boolean isAngleDecaying;
        if (carPositionsPrev == null
                || (Math.abs(carPositions.getCarPosition(ownCar).angle)
                    <  Math.abs(carPositionsPrev.getCarPosition(ownCar).angle)))
            isAngleDecaying = true;
        else isAngleDecaying = false;
        int currentPieceIndex = carPositions.getCarPosition(ownCar).piecePosition.pieceIndex;
        double ownSpeed = getSpeed(ownCar);
        if (isOwnCarSliding(35.0) && !isAngleDecaying) {
            send(new Throttle(0.1));
        } else if (
                ownSpeed < getSpeedLimit(getPieceAtDistance(currentPieceIndex, 2).radius)+1.6 &&
                ownSpeed < getSpeedLimit(getPieceAtDistance(currentPieceIndex, 1).radius)+0.55 &&
                ownSpeed < getSpeedLimit(getPieceAtDistance(currentPieceIndex, 0).radius)) {
            if (isOwnCarSliding(30.0) && !isAngleDecaying) {
                send(new Throttle(0.6));
            } else if (isOwnCarSliding(25.0) && !isAngleDecaying) {
                send(new Throttle(0.8));
            } else {
                send(new Throttle(1.0));
            }
        } else {
            send(new Throttle(0.0));
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        String json = new Gson().toJson(new MsgWrapper(this));
        if (Main.DEBUG_SENT) { System.out.println("sent @ " + Main.getGameTick() + ": " + json); }
        return json;
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class NewRace extends SendMsg {
    public final BotId botId;
    public final String trackName;
    public final int carCount;
    public final String password;
    private transient final String joinOrCreate;

    NewRace(final String name, final String key, String joinOrCreate, final String trackName, final int carCount, String password) {
        botId = new BotId(name, key);
        this.joinOrCreate = joinOrCreate;
        this.trackName = trackName;
        this.carCount = carCount;
        this.password = password;
    }

    @Override
    protected String msgType() {
        if ("join".equalsIgnoreCase(joinOrCreate)) {
            return "joinRace";
        } else {
            return "createRace";
        }
    }
}

class BotId {
    public final String name;
    public final String key;

    BotId(final String name, final String key) {
        this.name = name;
        this.key = key;
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class SwitchLane extends SendMsg {
    enum Direction {
        Left,
        Right
    }

    private Direction direction;

    public SwitchLane(Direction direction) {
        this.direction = direction;
        Main.switchCommandSent = true;
    }

    @Override
    protected Object msgData() {
        return direction.name();
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}
