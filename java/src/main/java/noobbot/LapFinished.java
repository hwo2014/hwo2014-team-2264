package noobbot;

public class LapFinished {
    public final String msgType;//"lapFinished"
    public final Data data;

    LapFinished(final String msgType, final Data data) {
        this.msgType = msgType;
        this.data = data;
    }

    public class Data {
        public final CarId car;
        public final LapTime lapTime;
        public final RaceTime raceTime;
        public final Ranking ranking;

        Data(final CarId car, final LapTime lapTime, final RaceTime raceTime, final Ranking ranking) {
            this.car = car;
            this.lapTime = lapTime;
            this.raceTime = raceTime;
            this.ranking = ranking;
        }
    }

    public class LapTime {
        public final int lap;
        public final int ticks;
        public final int millis;

        LapTime(final int lap, final int ticks, final int millis) {
            this.lap = lap;
            this.ticks = ticks;
            this.millis = millis;
        }
    }

    public class RaceTime {
        public final int laps;
        public final int ticks;
        public final int millis;

        RaceTime(final int laps, final int ticks, final int millis) {
            this.laps = laps;
            this.ticks = ticks;
            this.millis = millis;
        }
    }

    public class Ranking {
        public final int overall;
        public final int fastestLap;

        Ranking(final int overall, final int fastestLap) {
            this.overall = overall;
            this.fastestLap = fastestLap;
        }
    }

}
