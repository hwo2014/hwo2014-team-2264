package noobbot;

import java.util.List;

public class CarPositions {
    public final String msgType;//"carPositions"
    public final List<CarPosition> data;
    public final String gameId;
    public final int gameTick;

    CarPositions(final String msgType, final List<CarPosition> data, final String gameId, final int gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameId = gameId;
        this.gameTick = gameTick;
    }

    public CarPosition getCarPosition(CarId id) {
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).id.equals(id)) {
                return data.get(i);
            }
        }
        return null;
    }

    public class CarPosition {
        public final CarId id;
        public final double angle;
        public final PiecePosition piecePosition;

        CarPosition(final CarId id, final double angle, final PiecePosition piecePosition) {
            this.id = id;
            this.angle = angle;
            this.piecePosition = piecePosition;
        }
    }

    public class PiecePosition {
        public final int pieceIndex;
        public final double inPieceDistance;
        public final Lane lane;
        public final int lap;

        PiecePosition(final int pieceIndex, final double inPieceDistance, final Lane lane, final int lap) {
            this.pieceIndex = pieceIndex;
            this.inPieceDistance = inPieceDistance;
            this.lane = lane;
            this.lap = lap;
        }
    }

    public class Lane {
        public final int startLaneIndex;
        public final int endLaneIndex;

        Lane(final int startLaneIndex, final int endLaneIndex) {
            this.startLaneIndex = startLaneIndex;
            this.endLaneIndex = endLaneIndex;
        }

        public boolean isChanging() {
            return (this.startLaneIndex != this.endLaneIndex);
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Lane other = (Lane) obj;
            if (this.startLaneIndex != other.startLaneIndex) {
                return false;
            }
            if (this.endLaneIndex != other.endLaneIndex) {
                return false;
            }
            return true;
        }
    }

    public double getAngle(CarId carId) {
        for (int i = 0; i < this.data.size(); i++) {
            if (carId.equals(this.data.get(i).id)) {
                return this.data.get(i).angle;
            }
        }
        // correct car not found
        return 0;
    }

}
