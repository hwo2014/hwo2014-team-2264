package noobbot;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class TrackInformation {
    public final String msgType;//"gameInit"
    public final String gameId;
    public final Data data;

    TrackInformation(final String msgType, final Data data, final String gameId) {
        this.msgType = msgType;
        this.data = data;
        this.gameId = gameId;
    }

    public List<Piece> getPieces() {
        return data.race.track.pieces;
    }

    public Piece getPiece(int index) {
        return data.race.track.pieces.get(index);
    }

    public int getOuterLaneIndex(List<Piece> subTrack) {
        if (getInnerLaneIndex(subTrack) == 0) return data.race.track.lanes.size()-1;
        else return 0;
    }

    public int getInnerLaneIndex(List<Piece> subTrack) {
        double sumOfAngles = 0;
        for (int i=0; i < subTrack.size(); i++) {
            sumOfAngles += subTrack.get(i).angle;
        }
        if (sumOfAngles < 0) return 0;
        else return data.race.track.lanes.size()-1;
    }

    public int getNextSwitchIndex(int currentPieceIndex) {
        for (int i = currentPieceIndex + 1; i < getPieces().size(); i++ ) {
            if (getPieces().get(i).isSwitch) return i;
        }
        for (int i = 0; i < currentPieceIndex; i++ ) {
            if (getPieces().get(i).isSwitch) return i;
        }
        return 0;
    }

    public int getOptimalLaneBetweenSwitches(int startSwitchIndex, boolean shortest) {
        //TODO: optimal = this is far from perfect. It does not calculate lane lengths at all! :)
        List<Piece> subTrack;
        int endSwitchIndex = getNextSwitchIndex(startSwitchIndex);
        if (endSwitchIndex < startSwitchIndex) {
            subTrack = getPieces().subList(startSwitchIndex, getPieces().size());
            subTrack.addAll(getPieces().subList(0, endSwitchIndex));
        } else {
            subTrack = getPieces().subList(startSwitchIndex, endSwitchIndex);
        }
        //System.out.println("ShortestLine:subTrack:"+startSwitchIndex+"->"+endSwitchIndex);
        if (shortest) return getInnerLaneIndex(subTrack);
        else return getOuterLaneIndex(subTrack);

    }

    public class Data {
        public final Race race;

        Data(final Race race) {
            this.race = race;
        }
    }

    public class Race {
        public final Track track;
        public final List<Car> cars;
        public final RaceSession raceSession;

        Race(final Track track, final List<Car> cars, final RaceSession raceSession) {
            this.track = track;
            this.cars = cars;
            this.raceSession = raceSession;
        }
    }

    public class Track {
        public final String id;
        public final String name;
        public final List<Piece> pieces;
        public final List<Lane> lanes;
        public final StartingPoint startingPoint;

        Track(final String id, final String name, final List<Piece> pieces,
                final List<Lane> lanes, final StartingPoint startingPoint) {
            this.id = id;
            this.name = name;
            this.pieces = pieces;
            this.lanes = lanes;
            this.startingPoint = startingPoint;
        }
    }

    public class Piece {
        public final double length;
        public final double radius;
        public final double angle;
        @SerializedName("switch")
        public final boolean isSwitch;

        Piece(final double length, final double radius, final double angle, final boolean isSwitch) {
            this.length = length;
            this.radius = radius;
            this.angle = angle;
            this.isSwitch = isSwitch;
        }

        private Lane getLane(TrackInformation info, int index) {
            for (int i = 0; i < info.data.race.track.lanes.size(); i++) {
                Lane lane = info.data.race.track.lanes.get(i);
                if (lane.index == index) {
                    return lane;
                }
            }
            System.out.println("ERROR: no Lane found with index=" + index);
            return null;
        }

        /**
         * TODO: This does not get correct answers on switch pieces, at least when lane
         *       is changing. It is needed for accurate speedometer.
         */
        public double getLaneLength(TrackInformation info, CarPositions.Lane carLane) {
            if (length > 0.0) {
                // Straigth
                return length;
            } else {
                // Bend
                TrackInformation.Lane lane = getLane(info, carLane.endLaneIndex);
                // TODO: if (carLane.isChanging()) { use some other formula to calculate length }
                int distanceFromCenter;
                if (lane == null) {
                    distanceFromCenter = 0;
                } else {
                    distanceFromCenter = lane.distanceFromCenter;
                }
                // distanceFromCenter is smaller than 0 at left side of center line when driving forward
                if (this.angle > 0) distanceFromCenter = -distanceFromCenter;
                // arc length of sector
                return Math.abs(this.angle) / 360 * 2 * Math.PI * (this.radius + distanceFromCenter);
            }
        }

        @Override
        public String toString() {
            return "length: " + length +
                " radius: " + radius +
                " angle: " + angle +
                " switch: " + (isSwitch ? "true" : "false");
        }
    }

    public class Lane {
        public final int distanceFromCenter;
        public final int index;

        Lane(final int distanceFromCenter, final int index) {
            this.distanceFromCenter = distanceFromCenter;
            this.index = index;
        }
    }

    public class StartingPoint {
        public final Position position;
        public final double angle;

        StartingPoint(final Position position, final double angle) {
            this.position = position;
            this.angle = angle;
        }
    }

    public class Position {
        public final double x;
        public final double y;

        Position(final double x, final double y) {
            this.x = x;
            this.y = y;
        }
    }

    public class Car {
        public final CarId id;
        public final Dimension dimensions;

        Car(final CarId id, final Dimension dimensions) {
            this.id = id;
            this.dimensions = dimensions;
        }
    }

    public class Dimension {
        public final double length;
        public final double width;
        public final double guideFlagPosition;

        Dimension(final double length, final double width, final double guideFlagPosition) {
            this.length = length;
            this.width = width;
            this.guideFlagPosition = guideFlagPosition;
        }
    }

    /** Always check isQualifying first before reading other values */
    public class RaceSession {
        public final int laps;
        public final int maxLapTimeMs;
        public final int durationMs;
        public final boolean quickRace;
        public final boolean isQualifying;

        RaceSession(final int laps, final int maxLapTimeMs, final boolean quickRace) {
            this.laps = laps;
            this.maxLapTimeMs = maxLapTimeMs;
            this.quickRace = quickRace;
            this.isQualifying = false;

            this.durationMs = 0;
        }

        RaceSession(final int durationMs) {
            this.durationMs = durationMs;
            this.isQualifying = true;

            this.laps = 0;
            this.maxLapTimeMs = 0;
            this.quickRace = false;
        }
    }
}
