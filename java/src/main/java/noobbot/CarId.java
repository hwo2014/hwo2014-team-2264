package noobbot;

public class CarId {
    public final String name;
    public final String color;

    CarId(final String name, final String color) {
        this.name = name;
        this.color = color;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CarId other = (CarId) obj;
        if (!this.name.equals(other.name)) {
            return false;
        }
        if (!this.color.equals(other.color)) {
            return false;
        }
        return true;
    }
}
